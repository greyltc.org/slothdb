Project project_name {
  database_type: 'PostgreSQL'
  Note: 'slothdb database design'
}

enum device_type {
  pv
  LED
}

enum shape {
  circle
  rectangle
}

enum source {
  voltage
  current
}

enum user_type {
  superuser
  superreader
  user
  visitor
}

enum tco_type {
  ITO
  FTO
  AZO
}

//enum event_type {
//  NONE
//  SS
//  SUNSVOC
//  IV
//  MPPT
//}


Table tbl_users as U {
  id integer [pk, increment] // auto-increment
  name text
  created_at timestampz
  type user_type
  Note: 'One row per user'
}

Table tbl_run as R {
  id integer [pk, increment] // auto-increment
  user_id integer [ref: > U.id]
  start_date timestampz
  UUID uuid
  name text
  suns real
  recipe text
  duration interval
  placeholder integer [note: "TODO: many cols to add here"]
  Note: 'One row per time Start button pressed on setup'
}

Table tbl_substrates as S {
  id bigint [pk, increment] // auto-increment
  substrate_type_id integer [ref: > STYP.id]
  label text [unique]
  label_creation timestamp
  Note: 'One row per substrate labeled'
}

Table tbl_substrate_types as STYP {
  id integer [pk, increment] // auto-increment
  mfg text
  batch text
  tco_pattern_name text
  tco_type tco_type
  sheet_resistance real  [note: "ohm/sq"]
  t550 real [note: "percent transmission at 550nm"]
  Note: 'One per substrate type'
}

Table tbl_layouts as L {
  id integer [pk, increment] // auto-increment
  name text
  version text
  substrate_extents box
  Note: 'One row per unique layout design'
}

Table tbl_layout_devices as P {
  id integer [pk, increment] // auto-increment
  layout_id integer [ref: > L.id]
  number integer [note: "pixel number"]
  light_circle circle  [note: "dims are mm, for circular illuminated area"]
  dark_circle circle  [note: "dims are mm, for circular dark area"]
  light_outline path  [note: "dims are mm, illuminated outline area"]
  dark_outline path  [note: "dims are mm, dark outline area"]
  Note: 'One row per pixel on a layout design'
}

Table tbl_devices as D {
  substrate_id bigint  [ref: > S.id]
  layout_pixel_id integer [ref: > P.id]
  type device_type
  Note: 'One row per individual device fabricated, that is generally 6 per substrate'
}

Table tbl_setups as SUP {
  id integer [pk, increment] // auto-increment
  name text
  site_id omt [ref: > SITE.id]
  experiments jsonb
  Note: 'One row per measurement setup'
}

Table tbl_sites as SITE {
  id integer [pk, increment] // auto-increment
  name text
  Note: 'One row per site setup'
}

Table tbl_measurement_slot as SLOT {
  id integer [pk, increment] // auto-increment
  setup_id integer  [ref: > SUP.id]
  designator text
  offset point
  Note: 'One per measurement slot in a setup'
}

Table tbl_iv_event as IE {
  id bigint [pk, increment] // auto-increment
  run_id integer [ref: > R.id]
  slot_id integer [ref: > SLOT.id]
  layout_devices_id integer [ref: > P.id]
  n_points integer
  duration interval
  rate real
  fixed source [note: "TODO: check if this is redundant, redundant with status byte"]
  metadata_placeholder real [note: "TODO: many cols to add here"]
  Note: 'One row per JV scan executed'
}

Table tbl_ss_events as SSE {
  id bigint [pk, increment] // auto-increment
  run_id integer [ref: > R.id]
  slot_id integer [ref: > SLOT.id]
  layout_devices_id integer [ref: > P.id]
  n_points integer
  duration interval
  setpoint real
  fixed source [note: "TODO: check if this is redundant, redundant with status byte"]
  metadata_placeholder real [note: "TODO: many cols to add here"]
  Note: 'One row per steady-state dwell executed'
}

Table tbl_mppt_event as MPPTE {
  id bigint [pk, increment] // auto-increment
  run_id integer [ref: > R.id]
  slot_id integer [ref: > SLOT.id]
  layout_devices_id integer [ref: > P.id]
  n_points integer
  duration interval
  algorithm_name text
  algorithm_version text
  algorithm_parameters jsonb
  source source [note: "TODO: check if this is redundant, redundant with status byte"]
  metadata_placeholder real [note: "TODO: many cols to add here"]
  Note: 'One row per mppt interval executed'
}

Table tbl_smdat as SMDAT {
  id bigint [pk, increment] // auto-increment
  voltage real
  current real
  hardware_timestamp real
  status int
  Note: 'holds data points collected by any smu'
}

Table tbl_smus as SMUS {
  id integer [pk, increment] // auto-increment
  name text
  idn text
  Note: 'One row per smu'
}

Table tbl_slot_substrate as SSMAP {
    substrate_id bigint [ref: > S.id]
    run_id integer [ref: > R.id]
    slot_id integer [ref: > SLOT.id]
    Note: 'N rows per run where N is the number of slots loaded in that run'
}

Table tbl_slot_tool as STMAP {
    set_hash_id integer
    slot_id integer [ref: > SLOT.id]
    smu_id integer [ref: > SMUS.id]
    Note: 'describes how the data tools are wired to slots. if/when the wiring changes, the revision number is bumped'
}

Table tbl_slot_set_hashes as HASHES {
    set_hash_id integer
    hash int
    slot_id integer [ref: > SLOT.id]
    smu_id integer [ref: > SMUS.id]
    Note: 'describes how the data tools are wired to slots. if/when the wiring changes, the revision number is bumped'
}


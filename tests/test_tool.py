import unittest
from slothdb.dbtool import DBTool


class SlothDBTool(unittest.TestCase):
    """testing for slothDB DBTool class"""

    # must already exist. make it with `$ createdb testing`
    testing_db_name = "testing"

    # @unittest.skip("because this will never return")
    def test_plugin(self):
        """plugin mode testing"""
        actions = ["do_plugin_mode"]
        disk_db_url = f"postgresql:///{self.testing_db_name}"
        dbt = DBTool(actions=actions, disk_db_url=disk_db_url)
        dbt.run()

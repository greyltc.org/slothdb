import json
import math
import unittest

import psycopg

import slothdb.enums as sdbnums
import slothdb.geometry as slg
from slothdb.dbsync import SlothDBSync


class SlothDBSyncTestCase(unittest.TestCase):
    """testing for SlothDBSync class"""

    # two data points that could come from an SMU
    smudat = []
    smudat.append((2.4, 2.2, 3.4, 0))
    smudat.append((2.1, 2.3, 3.5, 1))

    # test user name
    uname = "jill likes to eat apples"

    # must already exist. make it with `$ createdb testing`
    testing_db_name = "testing"  # might also match the user's name

    run_params = {"us": 58.2, "them": "john", "those": None}

    def test_00_init(self):
        """factory and initilization tests"""
        db = SlothDBSync(db_name="testing")
        self.assertIsInstance(db, SlothDBSync)

    def test_01_connect(self):
        """test connect and close calls. a databse called 'testing' must exist"""
        db = SlothDBSync(db_name=self.testing_db_name)
        db.connect()
        self.assertIsInstance(db.connection, psycopg.Connection)
        self.assertFalse(db.connection.closed)
        db.close()
        self.assertIsInstance(db.connection, psycopg.Connection)
        self.assertTrue(db.connection.closed)

    def test_02_context(self):
        """test context usage"""
        with SlothDBSync(db_name=self.testing_db_name) as db:
            self.assertFalse(db.connection.closed)
        self.assertTrue(db.connection.closed)

    @unittest.skip("because will likely cause data loss, don't run by default")
    def test_03_table_init(self):
        """test table initialization DANGER OF DATA LOSS"""
        init_args = {}
        init_args["db_name"] = self.testing_db_name
        with SlothDBSync(**init_args) as db:
            ret_normal, ret_raw = db.setup_tbls(recreate_tables=True, redo_notify=True)
            self.assertEqual(ret_normal, 0)
            self.assertEqual(ret_raw, 0)

    def test_04_user_insert(self):
        """test user creation"""
        with SlothDBSync(db_name=self.testing_db_name) as db:
            self.assertGreaterEqual(uid1 := db.register_user(self.uname), 1)  # register a new user
            self.assertLess(uid2 := db.register_user(self.uname, expect_mod=True), 0)  # register a new user with the same name

            # undo what we just did in reverse
            # self.assertEqual(db.delete(f"{db.schema}.tbl_users", f"id = {uid1}"), 1)  # erase the test user

    def test_05_new_conf(self):
        """test new config and upsert"""
        conf = {"thinga": 1, "thingb": {"thingc": None}, "thingd": [22, 34.1]}
        new_note = "tst conf"

        with SlothDBSync(db_name=self.testing_db_name) as db:
            self.assertGreaterEqual(cid := db.register_conf(kind="a", conf=conf), 1)  # register a new conf
            self.assertEqual(db.register_conf(kind="a", conf=conf, note=new_note), cid)  # try to register the same conf again, but this time with a note
            # confirm that the config went in properly and that note was upserted properly too
            self.assertEqual(db.get("tbl_conf_as", ("conf", "note"), f"id = {cid}")[0], (conf, new_note))

            # undo what we just did in reverse
            # self.assertEqual(db.delete(f"{db.schema}.tbl_conf_as", f"id = {cid}"), 1)  # erase the conf

    def test_06_new_run(self):
        """test run start"""
        conf = {"thinga": 1, "thingb": {"thingc": None}, "thingd": [22, 34.1]}

        with SlothDBSync(db_name=self.testing_db_name) as db:
            self.assertGreaterEqual(uid := db.register_user(self.uname), 1)  # register a new user
            self.assertGreaterEqual(caid := db.register_conf(kind="a", conf=conf), 1)  # register a new conf
            self.assertGreaterEqual(cbid := db.register_conf(kind="b", conf=conf), 1)  # register a new conf
            self.assertGreaterEqual(rid := db.register_run(uid, caid, cbid), 1)  # register a new run

            # undo what we just did in reverse
            # self.assertEqual(db.delete(f"{db.schema}.tbl_runs", f"id = {rid}"), 1)  # erase the run
            # self.assertEqual(db.delete(f"{db.schema}.tbl_conf_bs", f"id = {cbid}"), 1)  # erase the conf
            # self.assertEqual(db.delete(f"{db.schema}.tbl_conf_as", f"id = {caid}"), 1)  # erase the conf
            # self.assertEqual(db.delete(f"{db.schema}.tbl_users", f"id = {uid}"), 1)  # erase the test user

    def test_07_events(self):
        """test interaction with event tables"""
        conf = {"thinga": 1, "thingb": {"thingc": None}, "thingd": [22, 34.1]}
        lo = {}  # layout
        lo["name"] = "lo1"
        lo["version"] = "0.1"
        lo["extents"] = slg.Box((-15.0, -15.0), (15.0, 15.0))
        lod = {}  # layout device/pixel
        lod["layout_id"] = None
        lod["pad_no"] = 1
        lod["light_cir"] = slg.Circle((-10.0, 10.0), float(math.sqrt(25.0 / math.pi)))
        lod["dark_cir"] = slg.Circle((-10.0, 10.0), float(math.sqrt(30.87 / math.pi)))
        ssrt = {}  # substrate
        ssrt["name"] = "DEADBEEF"  # etch label
        ssrt["layout_id"] = None
        ssrt["type_id"] = None
        dev = {}  # device
        dev["substrate_id"] = None
        dev["layout_device_id"] = None
        dev["type"] = sdbnums.DevType.SOLAR_CELL
        ss_info = {}
        ss_info["rid"] = None
        ss_info["did"] = None
        ss_info["fixed"] = sdbnums.Fixed.CURRENT
        ss_info["setpoint"] = 0.0
        ss_info["ea"] = None  # 1.2
        ss_info["complete"] = False
        with SlothDBSync(db_name=self.testing_db_name) as db:
            self.assertGreaterEqual(uid := db.register_user(self.uname), 1)  # register a new user
            self.assertGreaterEqual(caid := db.register_conf(kind="a", conf=conf), 1)  # register a new conf
            self.assertGreaterEqual(cbid := db.register_conf(kind="b", conf=conf), 1)
            self.assertGreaterEqual(lid := db.upsert("tbl_layouts", lo), 1)
            lod["layout_id"] = lid
            self.assertGreaterEqual(pid := db.upsert("tbl_layout_devices", lod), 1)
            self.assertGreaterEqual(gid := db.upsert("tbl_substrate_types", {}), 1)  # dummy row just to generate the id
            ssrt["layout_id"] = lid
            ssrt["type_id"] = gid
            ssrt["name"] = f'{ssrt["name"]}_{lid}_{gid}'
            self.assertGreaterEqual(sid := db.upsert("tbl_substrates", ssrt), 1)
            dev["substrate_id"] = sid
            dev["layout_device_id"] = pid
            self.assertGreaterEqual(did := db.upsert("tbl_devices", dev), 1)
            self.assertGreaterEqual(rid := db.register_run(uid, caid, cbid), 1)  # register a new run
            ss_info["rid"] = rid
            ss_info["did"] = did
            self.assertGreaterEqual(eid := db.new_ss_event(**ss_info), 1)  # register a new (incomplete) event
            self.assertEqual(db.complete_event(eid, sdbnums.Event.SS), eid)  # mark the event complete

            # undo what we just did in reverse
            # self.assertEqual(db.delete(f"tbl_ss_events", f"id = {eid}"), 1)  # erase the event
            # self.assertEqual(db.delete(f"tbl_runs", f"id = {rid}"), 1)  # erase the run
            # self.assertEqual(db.delete(f"tbl_devices", f"id = {did}"), 1)
            # self.assertEqual(db.delete(f"tbl_substrates", f"id = {sid}"), 1)
            # self.assertEqual(db.delete(f"tbl_substrate_types", f"id = {gid}"), 1)
            # self.assertEqual(db.delete(f"tbl_layout_devices", f"id = {pid}"), 1)
            # self.assertEqual(db.delete(f"tbl_layouts", f"id = {lid}"), 1)
            # self.assertEqual(db.delete(f"tbl_conf_bs", f"id = {cbid}"), 1)  # erase the conf
            # self.assertEqual(db.delete(f"tbl_conf_as", f"id = {caid}"), 1)  # erase the conf
            # self.assertEqual(db.delete(f"tbl_users", f"id = {uid}"), 1)  # erase the test user

    def test_08_data_insert(self):
        """test data insertion"""
        raw_tbl_cntr = 1
        conf = {"thinga": 1, "thingb": {"thingc": None}, "thingd": [22, 34.1]}
        lo = {}  # layout
        lo["name"] = "lo1"
        lo["version"] = "0.1"
        lo["extents"] = slg.Box((-15.0, -15.0), (15.0, 15.0))
        lod = {}  # layout device/pixel
        lod["layout_id"] = None
        lod["pad_no"] = 1
        lod["light_cir"] = slg.Circle((-10.0, 10.0), float(math.sqrt(25.0 / math.pi)))
        lod["dark_cir"] = slg.Circle((-10.0, 10.0), float(math.sqrt(30.87 / math.pi)))
        ssrt = {}  # substrate
        ssrt["name"] = "DEADBEEF"  # etch label
        ssrt["layout_id"] = None
        ssrt["type_id"] = None
        dev = {}  # device
        dev["substrate_id"] = None
        dev["layout_device_id"] = None
        dev["type"] = sdbnums.DevType.SOLAR_CELL
        ss_info = {}
        ss_info["rid"] = None
        ss_info["did"] = None
        ss_info["fixed"] = sdbnums.Fixed.CURRENT
        ss_info["setpoint"] = 0.0
        ss_info["ea"] = None  # 1.2
        ss_info["complete"] = False
        with SlothDBSync(db_name=self.testing_db_name) as db:
            # self.assertGreaterEqual(iid := db.register_site("The Moon"), 1)  # register a new site
            # self.assertGreaterEqual(tid := db.register_setup("R&D Testbed 2.0", iid), 1)  # register a new setup
            self.assertGreaterEqual(uid := db.register_user(self.uname), 1)  # register a new user
            self.assertGreaterEqual(caid := db.register_conf(kind="a", conf=conf), 1)  # register a new conf
            self.assertGreaterEqual(cbid := db.register_conf(kind="b", conf=conf), 1)
            self.assertGreaterEqual(lid := db.upsert("tbl_layouts", lo), 1)
            lod["layout_id"] = lid
            self.assertGreaterEqual(pid := db.upsert("tbl_layout_devices", lod), 1)
            self.assertGreaterEqual(gid := db.upsert("tbl_substrate_types", {}), 1)  # dummy row just to generate the id
            ssrt["layout_id"] = lid
            ssrt["type_id"] = gid
            ssrt["name"] = f'{ssrt["name"]}_{lid}_{gid}'
            self.assertGreaterEqual(sid := db.upsert("tbl_substrates", ssrt), 1)
            dev["substrate_id"] = sid
            dev["layout_device_id"] = pid
            self.assertGreaterEqual(did := db.upsert("tbl_devices", dev), 1)
            self.assertGreaterEqual(rid := db.register_run(uid, caid, cbid), 1)  # register a new run
            ss_info["rid"] = rid
            ss_info["did"] = did
            self.assertGreaterEqual(eid := db.new_ss_event(**ss_info), 1)  # register a new (incomplete) event
            self.assertEqual(db.putsmdat([(3.4, 21.4, 4, 34)], eid, sdbnums.Event.SS, raw_tbl_cntr), 1)
            self.assertEqual(db.putsmdat([(3.4, 21.4, 4, 34)], eid, sdbnums.Event.SS, raw_tbl_cntr), 1)
            # self.assertEqual(db.delete(f"tbl_ss_raw_1", ""), 2)  # erase ALL steady state table data
            # self.assertEqual(db.pg_cmd([f"ALTER SEQUENCE IF EXISTS rds RESTART"]), 0)  # test run counter reset
            self.assertEqual(db.putsmdat([(3.4, 21.4, 4, 34)], eid, sdbnums.Event.SS, 1), 1)
            self.assertEqual(db.putsmdat([(3.4, 21.4, 4, 34)] * 10, eid, sdbnums.Event.SS, 1), 10)  # test multi insert (uses COPY)
            self.assertEqual(db.complete_event(eid, sdbnums.Event.SS), eid)  # mark the event complete

            # undo what we just did in reverse
            # self.assertEqual(db.delete(f"tbl_ss_raw_1", ""), 11)  # erase ALL steady state table data
            # self.assertEqual(db.delete(f"tbl_ss_events", f"id = {eid}"), 1)  # erase the event
            # self.assertEqual(db.delete(f"tbl_runs", f"id = {rid}"), 1)  # erase the run
            # self.assertEqual(db.delete(f"tbl_devices", f"id = {did}"), 1)
            # self.assertEqual(db.delete(f"tbl_substrates", f"id = {sid}"), 1)
            # self.assertEqual(db.delete(f"tbl_substrate_types", f"id = {gid}"), 1)
            # self.assertEqual(db.delete(f"tbl_layout_devices", f"id = {pid}"), 1)
            # self.assertEqual(db.delete(f"tbl_layouts", f"id = {lid}"), 1)
            # self.assertEqual(db.delete(f"tbl_conf_bs", f"id = {cbid}"), 1)  # erase the conf
            # self.assertEqual(db.delete(f"tbl_conf_as", f"id = {caid}"), 1)  # erase the conf
            # self.assertEqual(db.delete(f"tbl_users", f"id = {uid}"), 1)  # erase the test user
            # self.assertEqual(db.delete(f"tbl_sites", f"id = {iid}"), 1)

    def test_09_upsert(self):
        """test insert and update with child events tables"""
        lo = {}  # layout
        lo["name"] = "lo1"
        lo["version"] = "0.1"
        lo["extents"] = slg.Box((-15.0, -15.0), (15.0, 15.0))
        with SlothDBSync(db_name=self.testing_db_name) as db:
            self.assertGreaterEqual(lid := db.upsert("tbl_layouts", lo), 1)
            to_upsert = {}
            to_upsert["version"] = "0.99"
            self.assertEqual(db.upsert("tbl_layouts", to_upsert, lid), lid)
            self.assertEqual(db.get("tbl_layouts", ("version",), f"id = {lid}")[0][0], to_upsert["version"])
            to_upsert = {}
            to_upsert["version"] = lo["version"]
            self.assertEqual(db.upsert("tbl_layouts", to_upsert, lid), lid)
            self.assertEqual(db.vac(), 0)  # test VACUUM

            # undo what we just did in reverse
            # self.assertEqual(db.delete(f"tbl_layouts", f"id = {lid}"), 1)

    @unittest.skip("because a special data source needs to be running for this to generatite notifications, otherwise it'll hang forever")
    def test_10_listening(self):
        """test listen/notify context"""
        n = 3  # let's get this many notifications before we're done
        init_args = {}
        init_args["db_name"] = self.testing_db_name
        with SlothDBSync(**init_args) as db:
            listen_channels = []
            listen_channels.append(f"{db.schema}_tbl_events")
            listen_channels.append(f"{db.schema}_raw_s1738994")
            with db.listen_notify_ctx(listen_channels):
                gen = db.connection.notifies()
                for notify in gen:
                    self.assertTrue(hasattr(notify, "payload"))
                    self.assertIsInstance(notify.payload, str)
                    rslt = json.loads(notify.payload)
                    self.assertIsInstance(rslt, dict)
                    n -= 1
                    if n == 0:
                        break

    def test_11_native_geometry(self):
        """tests all the native geometry types&adapters"""
        put = {}
        put["num"] = 100
        put["p"] = slg.Point((3, 2))
        put["lin"] = slg.Line(2, 3.1, 4)
        put["lins"] = slg.LineSegment((3, 2), (8.9, 10))
        put["bx"] = slg.Box((8.9, 10), (-96.2, 2))
        put["cir"] = slg.Circle((2.0, 3.0), 4.0)
        put["cpth"] = slg.Path(((2.0, 3.0), (2, 6.0), (2, 7.6), (2, 3)), True)
        put["opth"] = slg.Path(((2.0, 3.0), (2, 6.0), (2, 7.6)), False)
        put["poly"] = slg.Polygon(((65.2, 28), (44, 1), (-98.33, 21)))
        put["data"] = "abc'def"

        with SlothDBSync(db_name=self.testing_db_name) as db:
            this_table = f"{db.schemadot}geom"
            mktbl_cmd = f"""
                CREATE TABLE {this_table} (
                    id serial PRIMARY KEY,
                    num integer,
                    p point,
                    lin line,
                    lins lseg,
                    bx box,
                    cir circle,
                    cpth path,
                    opth path,
                    poly polygon,
                    data text)
                """

            self.assertEqual(db.pg_cmd([mktbl_cmd]), 0)

            self.assertGreaterEqual(gid := db.upsert(this_table, put), 1)
            self.assertTrue(got := db.get(this_table, condition=f"id = {gid}"))
            self.assertEqual(got[0], (gid,) + tuple(put.values()))

            self.assertEqual(db.pg_cmd([f"DROP TABLE {this_table}"]), 0)

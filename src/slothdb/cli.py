#!/usr/bin/env python3

import sys
from slothdb.__main__ import cli


def main():
    cli(sys.argv[1:], sys.argv[0])


if __name__ == "__main__":
    main()

#!/usr/bin/env python3

from slothdb.dbsync import SlothDBSync
from slothdb import geometry as slg
import os
import redis
import yaml
import json
import math
import sys
import importlib.metadata


class DBTool(object):
    """higher level interface class for DB interaction"""

    ret_code: int = 0
    disk_db_url: str = "postgresql://"
    mem_db_url: str = "redis://"
    mem_db_url_env_name: str = "MEM_DB_URL"
    disk_db_url_env_name: str = "DISK_DB_URL"
    actions = []

    def __init__(self, actions=[], mem_db_url: None | str = None, disk_db_url: None | str = None):
        self.actions = actions

        if mem_db_url:
            self.mem_db_url = mem_db_url
        elif self.mem_db_url_env_name in os.environ:
            self.mem_db_url = os.environ[self.mem_db_url_env_name]

        if disk_db_url:
            self.disk_db_url = disk_db_url
        elif self.disk_db_url_env_name in os.environ:
            self.disk_db_url = os.environ[self.disk_db_url_env_name]

    def run(self) -> int:
        """completes database-related actions"""
        with SlothDBSync(db_uri=self.disk_db_url) as db:
            to_dump = []
            for action in self.actions:
                if action == "dump_configs":
                    got = db.get("tbl_conf_as", ("id", "conf"))
                    if got:
                        for item in got:
                            to_dump.append({str(item[0]): item[1]})
                elif action == "do_plugin_mode":
                    self.do_plugin_mode(db)
                else:
                    print(f"Unrecognized action: {action}")

            print("DB actions complete.")

            for conf in to_dump:
                id = next(iter(conf))
                with open(f"{id}.yaml", "w") as yc:
                    print(f"Dumping {yc.name}")
                    yaml.safe_dump(conf[id], yc, default_flow_style=False)

        return self.ret_code

    def do_plugin_mode(self, db: SlothDBSync):
        """runs a redis listner that syncs everything with postgres"""
        with redis.Redis.from_url(self.mem_db_url) as r:
            with r.monitor() as m:
                # mappings between memdb ids (keys) and diskdb ids (vals)
                mappings: dict[str, dict[int, int]] = {
                    "conf_aid": {},
                    "conf_bid": {},
                    "conf_cid": {},
                    "rid": {},
                }
                print("Running in plugin-mode. Terminate with Ctrl-C")
                for c in m.listen():  # all commands stream through here
                    print(c)
                    try:
                        stream = dict(zip(("cmd", "channel", "args"), c["command"].split(" ", 2)))
                        if stream["cmd"] == "ZRANK":
                            if stream["channel"].startswith("conf_"):
                                if stream["channel"].endswith("s"):
                                    DBTool.reg_conf(db, stream["channel"], stream["args"])
                        elif stream["cmd"] == "HSET":
                            if stream["channel"].startswith("conf_"):
                                if stream["channel"].endswith("_notes"):
                                    # handle adding a note to a conf
                                    conf_type = stream["channel"].removeprefix("conf_").removesuffix("_notes")
                                    rdids, note = stream["args"].split(" ", 1)
                                    rdid = int(rdids)
                                    conf = json.loads(r.zrange(f"conf_{conf_type}s", rdid, rdid)[0])
                                    assert (ddid := db.register_conf(kind=conf_type, conf=conf, note=note)) > 0, "Could not handle note update"
                                    mappings["conf_aid"][rdid] = ddid
                            elif stream["channel"].startswith("run:"):
                                rdid = int(stream["channel"].removeprefix("run:"))
                                arg_split = stream["args"].split(" ")
                                arg_dict = dict(zip(arg_split[::2], arg_split[1::2]))
                                if rdid in mappings["rid"]:
                                    # mod an existing run line into an existing run
                                    # NOTE: we might get in trouble with data types here
                                    assert db.upsert(f"tbl_runs", arg_dict, id=mappings["rid"][rdid]) > 0, "Could not handle run update"
                                else:
                                    # new run entry
                                    to_upsert = {}
                                    to_upsert["conf_a_id"] = int(arg_dict["conf_a_id"])
                                    confb_rdid = int(arg_dict["conf_b_id"])
                                    to_upsert["conf_b_id"] = confb_rdid
                                    assert (conf_bb := r.lindex(f"conf_bs", confb_rdid)), "Unable to lookup conf_b"
                                    conf_b = json.loads(conf_bb)
                                    user_name = conf_b["user_name"]
                                    assert (user_ddid := db.register_user(user_name)) > 0, f"Unable to look up user: {user_name}"
                                    to_upsert["user_id"] = user_ddid
                                    # to_upsert["conf_c_id"] = arg_dict["conf_c_id"] TODO
                                    to_upsert["dbsw_ver"] = importlib.metadata.version("slothdb")
                                    to_upsert["backend_ver"] = "0.0.0"  # TODO: remove this when NULL becomes valid
                                    # to_upsert["frontend_ver"] = arg_dict["frontend_ver"] TODO
                                    # to_upsert["started"] = bool(int(arg_dict["started"])) TODO
                                    to_upsert["complete"] = bool(int(arg_dict["complete"]))
                                    assert (ddid := db.upsert(f"tbl_runs", to_upsert)) > 0, f"Could not handle run insert"
                                    mappings["rid"][rdid] = ddid
                        elif stream["cmd"] == "RPUSH":
                            if stream["channel"].startswith("conf_"):
                                if stream["channel"].endswith("s"):
                                    DBTool.reg_conf(db, stream["channel"], stream["args"])
                        elif stream["cmd"] == "PUBLISH":
                            if stream["channel"].startswith("conf_"):
                                if stream["channel"].endswith("s:new"):
                                    conf_type = stream["channel"].removeprefix("conf_").removesuffix("s:new")
                                    if conf_type in ("a", "b"):  # TODO: type c not yet supported by disk-based database
                                        rdid = int(stream["args"])
                                        if rdid not in mappings[f"conf_{conf_type}id"]:
                                            confb = None
                                            if conf_type == "a":
                                                confb = r.zrange(f"conf_{conf_type}s", rdid, rdid)[0]
                                            elif conf_type == "b":  # TODO: change conf b storage in memdb to store them as sortedset types like a, instead of list
                                                confb = r.lindex(f"conf_{conf_type}s", rdid)
                                            if confb:
                                                conf = json.loads(confb)
                                                assert (ddid := db.register_conf(kind=conf_type, conf=conf)) > 0, f"Could not lookup id"
                                                mappings[f"conf_{conf_type}id"][rdid] = ddid

                        # elif msg["channel"] == "runs:new":
                        #     conf_a = json.loads(r.zrange("conf_as", this["conf_a_id"], this["conf_a_id"])[0])
                        #     conf_b = json.loads(r.zrange("conf_bs", this["conf_b_id"], this["conf_b_id"])[0])
                        #     layouts = conf_a["substrates"]["layouts"]
                    except Exception as e:
                        print(f"Command error: {repr(e)}", file=sys.stderr)

    @staticmethod
    def reg_conf(db: SlothDBSync, channel: str, args: str) -> int | None:
        id = None
        conf_type = channel.removeprefix("conf_").removesuffix("s")
        if conf_type != "c":  # TODO: type c not yet supported by disk-based database
            conf = json.loads(args)
            assert (id := db.register_conf(kind=conf_type, conf=conf)) > 0, f"Could not register conf_{conf_type}"
        return id


if __name__ == "__main__":
    dbt = DBTool()
    dbt.actions = ["do_plugin_mode"]
    dbt.run()

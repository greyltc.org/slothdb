#!/usr/bin/env python3

import gi

gi.require_version("Adw", "1")
gi.require_version("Gtk", "4.0")
from gi.repository import GLib, Gtk, Gio, GObject, Adw, Gdk, GdkPixbuf, Pango
from slothdb.dbtool import DBTool


def main():
    dbt = DBTool()
    dbt.run()
    loop = GLib.MainLoop.new(context=None, is_running=False)

    msg = "<b>Not Implemented</b>"
    sub_msg = "at this time"
    # # === GTK4.10 only ====
    # # alert = Gtk.AlertDialog(msg)
    # # === not GTK4.10 ====
    alert = Gtk.MessageDialog(message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK)
    alert.props.buttons = Gtk.ButtonsType.OK
    alert.props.use_markup = True
    alert.props.secondary_use_markup = True
    alert.props.text = msg
    alert.props.secondary_text = sub_msg

    alert.connect("response", lambda a, b: loop.quit())
    alert.present()
    loop.quit()

    loop.run()


if __name__ == "__main__":
    main()

import psycopg
from dataclasses import dataclass
from psycopg.adapt import Loader, Dumper
import ast


@dataclass(frozen=True)
class Point:
    xy: tuple[float, float]


class PointDumper(Dumper):
    oid = psycopg.adapters.types["point"].oid

    def dump(self, po: Point) -> bytes:
        return f"({po.xy[0]},{po.xy[1]})".encode()


class PointLoader(Loader):
    def load(self, co: memoryview) -> Point:
        bo = bytes(co.obj)
        evald = ast.literal_eval(bo.decode())
        return Point(evald)


@dataclass(frozen=True)
class Line:
    A: float
    B: float
    C: float


class LineDumper(Dumper):
    oid = psycopg.adapters.types["line"].oid

    def dump(self, lo: Line) -> bytes:
        return f"{{{lo.A},{lo.B},{lo.C}}}".encode()


class LineLoader(Loader):
    def load(self, co: memoryview) -> Line:
        bo = bytes(co.obj).strip(b"{}")
        evald = ast.literal_eval(bo.decode())
        return Line(*evald)


@dataclass(frozen=True)
class LineSegment:
    xy1: tuple[float, float]
    xy2: tuple[float, float]


class LineSegmentDumper(Dumper):
    oid = psycopg.adapters.types["lseg"].oid

    def dump(self, lso: LineSegment) -> bytes:
        return f"[{lso.xy1},{lso.xy2}]".encode()


class LineSegmentLoader(Loader):
    def load(self, co: memoryview) -> LineSegment:
        bo = bytes(co.obj).strip(b"[]")
        evald = ast.literal_eval(bo.decode())
        return LineSegment(*evald)


@dataclass(frozen=True)
class Box:
    xy1: tuple[float, float]
    xy2: tuple[float, float]


class BoxDumper(Dumper):
    oid = psycopg.adapters.types["box"].oid

    def dump(self, bo: Box) -> bytes:
        return f"{bo.xy1},{bo.xy2}".encode()


class BoxLoader(Loader):
    def load(self, co: memoryview) -> Box:
        bo = bytes(co.obj)
        evald = ast.literal_eval(bo.decode())
        return Box(*evald)


@dataclass(frozen=True)
class Path:
    xys: tuple[tuple[float, float], ...]
    closed: bool = True


class PathDumper(Dumper):
    oid = psycopg.adapters.types["path"].oid

    def dump(self, po: Path) -> bytes:
        if po.closed:
            return f"{po.xys}".encode()
        else:
            return f"{list(po.xys)}".encode()


class PathLoader(Loader):
    def load(self, co: memoryview) -> Path:
        bo = bytes(co.obj)
        evald = ast.literal_eval(bo.decode())
        if isinstance(evald, tuple):
            closed = True
        else:
            evald = tuple(evald)
            closed = False
        return Path(evald, closed)


@dataclass(frozen=True)
class Polygon:
    xys: tuple[tuple[float, float], ...]


class PolygonDumper(Dumper):
    oid = psycopg.adapters.types["polygon"].oid

    def dump(self, po: Polygon) -> bytes:
        return f"{po.xys}".encode()


class PolygonLoader(Loader):
    def load(self, co: memoryview) -> Polygon:
        bo = bytes(co.obj)
        evald = ast.literal_eval(bo.decode())
        return Polygon(evald)


@dataclass(frozen=True)
class Circle:
    center: tuple[float, float]
    radius: float


class CircleDumper(Dumper):
    oid = psycopg.adapters.types["circle"].oid

    def dump(self, co: Circle) -> bytes:
        return f"<{co.center},{co.radius}>".encode()


class CircleLoader(Loader):
    def load(self, co: memoryview) -> Circle:
        bo = bytes(co.obj).strip(b"<>")
        evald = ast.literal_eval(bo.decode())
        return Circle(*evald)

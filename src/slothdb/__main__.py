#!/usr/bin/env python3

import sys
from slothdb.dbtool import DBTool
import argparse
from typing import Optional, Sequence


def _get_main_parser() -> argparse.ArgumentParser:
    """Construct the main parser."""
    parser = argparse.ArgumentParser(description="CLI for the sloth DB")

    parser.add_argument(
        "--disk-db-url",
        help="Disk-based database connection string",
    )

    parser.add_argument(
        "--mem-db-url",
        help="Memory-based database connection string (for plugin mode)",
    )

    parser.add_argument(
        "--dump-configs",
        action="store_true",
        help="Dump all known configs to files (existing files may be overwritten)",
    )

    parser.add_argument(
        "--plugin-mode",
        action="store_true",
        help="Run in database plugin mode",
    )

    return parser


def cli(cli_args: Sequence[str] | None = None, program: Optional[str] = None) -> None:
    parser = _get_main_parser()

    if program:
        parser.prog = program
    parser.epilog = f"example usage: {parser.prog} --dump-configs"

    if cli_args:
        args = parser.parse_args(cli_args)
    else:
        args = parser.parse_args()

    actions = []
    if args.dump_configs:
        actions.append("dump_configs")
    if args.plugin_mode:
        actions.append("do_plugin_mode")

    dbt_init_args = {}
    dbt_init_args["actions"] = actions
    if args.disk_db_url:
        dbt_init_args["disk_db_url"] = args.disk_db_url
    if args.mem_db_url:
        dbt_init_args["mem_db_url"] = args.mem_db_url

    dbt = DBTool(**dbt_init_args)
    dbt.run()


if __name__ == "__main__":
    # cli(sys.argv[1:], "python -m slothdb")
    # cli(["--dump-configs"], "python -m slothdb")
    # cli(["--dump-configs", "--disk-db-url", ""], "python -m slothdb")
    cli(["--plugin-mode", "--disk-db-url", "postgresql://127.0.0.1/testing"], "python -m slothdb")

import contextlib
import getpass
import importlib.metadata
from typing import Any, Generator, Iterable

import psycopg
from psycopg.types.json import Jsonb

import slothdb.enums as en
import slothdb.geometry as slg
from slothdb.logstuff import get_logger
import math


class SlothDBSync(object):
    """synchronous (non-async) DB interface"""

    db_proto = "postgresql://"
    db_user = None
    db_name = None
    db_host = None
    db_port = 5432
    db_uri = None
    connection: psycopg.Connection
    force_rollback = False  # forces all transactions to be rolled back instead of committed, good for testing
    schema = "org_greyltc"
    dat_seq: Generator[int, int | None, None]

    def __init__(self, db_proto=db_proto, db_user=db_user, db_name=db_name, db_host=db_host, db_port=db_port, db_uri=db_uri):
        self.lg = get_logger(".".join([__name__, type(self).__name__]))  # setup logging
        # https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING
        if db_uri is not None:  # all connection params are overwritten by using this
            self.db_uri = db_uri
        else:
            if db_proto is None:
                self.db_proto = "postgresql://"
            else:
                self.db_proto = db_proto
            self.db_uri = self.db_proto
            if db_user is None:
                self.db_user = getpass.getuser()
            else:
                self.db_user = db_user
            self.db_uri = self.db_uri + f"{self.db_user}@"
            if db_host is None:
                self.db_host = ""
            else:
                self.db_host = db_host
            self.db_uri = self.db_uri + self.db_host
            if self.db_host != "":
                if db_port is None:
                    self.db_port = 5432
                else:
                    self.db_port = db_port
                self.db_uri = self.db_uri + f":{self.db_port}"
            if db_name is None:
                self.db_name = getpass.getuser()
            else:
                self.db_name = db_name
            self.db_uri = self.db_uri + f"/{self.db_name}"
        if self.schema == "":
            self.schemadot = ""
        else:
            self.schemadot = f"{self.schema}."

        self.dat_seq = SlothDBSync.counter_sequence()

    def __enter__(self) -> "SlothDBSync":
        """used to make this work with context managers"""
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> bool:
        # self.commit()  # everything is in transaction blocks so commits are taken care of
        self.close()
        return False

    @staticmethod
    def counter_sequence(start: int = 0) -> Generator[int, int | None, None]:
        """infinite upwards integer sequence generator"""
        c = start
        while True:
            yield c
            c += 1

    def connect(self, autocommit: bool = True) -> int:
        """form connection to database"""
        try:
            if (not hasattr(self, "connection")) or (isinstance(self.connection, psycopg.Connection) and self.connection.closed):
                if self.db_uri:
                    self.connection = psycopg.Connection.connect(self.db_uri, autocommit=autocommit)
        except Exception as e:
            self.lg.debug(f"Problem connecting to database: {repr(e)}")
            ret = -1
        else:
            ret = 0
        return ret

    def commit(self) -> int:
        """Commit any pending transaction to the db"""
        if hasattr(self, "connection") and not self.connection.closed:
            self.connection.commit()
            ret = 0
        else:
            self.lg.debug(f"Can't commit because there's no open connection.")
            ret = -1
        return ret

    def close(self) -> None:
        try:
            if self.connection:
                self.connection.close()
        except Exception as e:
            self.lg.debug(f"Problem closing connection to database: {repr(e)}")
        return None

    def pg_cmd(self, cmds: list[str]) -> int:
        """run a list of commands one after another"""
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
                        for cmd in cmds:
                            cur.execute(cmd)
            except Exception as e:
                self.lg.debug(f"Problem running pg_cmd: {repr(e)}")
                ret = -1
            else:
                ret = 0
        else:
            ret = -2
        return ret

    def ask(self, sql_query: str) -> tuple[int, list]:
        """run a sql query and return the result(s)"""
        rslt = []
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
                        cur.execute(sql_query)
                        rslt = cur.fetchall()
            except Exception as e:
                self.lg.debug(f"Problem running ask: {repr(e)}")
                ret = -1
            else:
                ret = 0
        else:
            ret = -2
        return (ret, rslt)

    def putsmdat(self, data: list[tuple[float, float, float, int]], eid: int, kind: en.Event, table_counter: int = 1) -> int:
        """insert data row into a raw data table"""
        to_put = []
        tbl = f"tbl_{kind.value}_raw_{table_counter}"
        col_names = ["eid", "dcs", "v", "i", "t", "s"]
        for row in data:
            to_put.append((eid, next(self.dat_seq)) + row)
        return self.multiput(tbl, to_put, col_names)

    def multiput(self, table: str, data: list[tuple], col_names: list[str]) -> int:
        """insert large data into a table in an efficient way"""
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
                        table = table.removeprefix(self.schemadot)
                        tables = f"{self.schemadot}{table}"
                        nrows = len(data)
                        if nrows < 2:
                            for row in data:
                                new = {}
                                for i, col in enumerate(col_names):
                                    new[col] = row[i]
                                cur.execute(f'INSERT INTO {tables} ({", ".join(col_names)}) VALUES (%({")s, %(".join(col_names)})s)', new)
                        elif nrows >= 2:  # for bigger entries use copy for speed
                            with cur.copy(f'COPY {tables} ({", ".join(col_names)}) FROM STDIN') as copy:
                                for row in data:
                                    copy.write_row(row)

            except Exception as e:
                self.lg.debug(f"Problem inserting data: {repr(e)}")
                ret = -1
            else:
                ret = nrows
        else:
            ret = -2
        return ret

    # def putsmdat(self, data: list[tuple[float, float, float, int]], eid: int, kind: en.Event, table_counter: int = 1) -> int:
    #     """insert data row into a raw data table"""
    #     if hasattr(self, "connection") and not self.connection.closed:
    #         try:
    #             with self.connection.cursor() as cur:
    #                 with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
    #                     tbl = f"tbl_{kind.value}_raw_{table_counter}"  # table counter should probably be site id
    #                     nrows = len(data)
    #                     if nrows < 2:
    #                         for row in data:
    #                             new = {"eid": eid, "dcs": next(self.dat_seq), "v": row[0], "i": row[1], "t": row[2], "s": row[3]}
    #                             cur.execute(f"INSERT INTO {self.schemadot}{tbl} (eid, dcs, v, i, t, s) VALUES (%(eid)s, %(dcs)s, %(v)s, %(i)s, %(t)s, %(s)s)", new)
    #                             # cur.execute(f"INSERT INTO {self.schemadot}{tbl} (eid, v, i, t, s) VALUES (%(eid)s, %(v)s, %(i)s, %(t)s, %(s)s)", new)
    #                     elif nrows >= 2:  # for bigger entries use copy for speed
    #                         # cur.execute(f"ALTER TABLE {self.schemadot}{tbl} ALTER COLUMN dcs SET DEFAULT nextval('dat_seq')")
    #                         with cur.copy(f"COPY {self.schemadot}{tbl} (eid, dcs, v, i, t, s) FROM STDIN") as copy:
    #                             for i, record in enumerate(data):
    #                                 copy.write_row((eid, next(self.dat_seq)) + record)
    #                         # cur.execute(f"ALTER TABLE {self.schemadot}{tbl} ALTER COLUMN dcs DROP DEFAULT")

    #         except Exception as e:
    #             self.lg.debug(f"Problem inserting data: {repr(e)}")
    #             ret = -1
    #         else:
    #             ret = nrows
    #     else:
    #         ret = -2
    #     return ret

    def register_site(self, name: str) -> int:
        """register new/fetch existing site"""
        return self.upsert(f"tbl_sites", {"name": name})

    def register_setup(self, name: str, sid: int, exps: dict | None = None) -> int:
        """register new/fetch existing setup"""
        return self.upsert(f"tbl_setups", {"name": name, "site_id": sid, "experiments": Jsonb(exps)})

    def register_user(self, login: str, email: str | None = None, kind: en.UserTypes = en.UserTypes.USER, expect_mod: bool = False) -> int:
        """register new/fetch existing user"""
        return self.upsert(f"tbl_users", {"name": login, "email": email, "kind": kind}, expect_mod=expect_mod)

    def register_conf(self, conf: dict, kind: str = "a", note: str | None = None) -> int:
        """register a new config. kind can be a, for things that change rarely or b for things that change often"""
        id = self.upsert(f"tbl_conf_{kind}s", {"conf": Jsonb(conf), "note": note})
        if id > 0:  # conf went in okay
            if kind == "a":
                if "setup" in conf:
                    if "site" in conf["setup"]:
                        sid = self.register_site(conf["setup"]["site"])
                        assert sid > 0, "site registration fail"
                        if "site" in conf["setup"]:
                            if ("motion" in conf) and ("centers" in conf["motion"]):
                                centers = conf["motion"]["centers"]
                            else:
                                centers = None
                            seid = self.register_setup(conf["setup"]["name"], sid, exps=centers)
                            assert seid > 0, "setup registration fail"
                            smuids = []
                            if "smus" in conf:
                                for smu in conf["smus"]:
                                    to_upsert = {}  # a data tool
                                    to_upsert["setup_id"] = seid
                                    to_upsert["address"] = smu["address"]
                                    dtid = self.upsert("tbl_tools", to_upsert)
                                    assert dtid > 0, "data tool registration fail"
                                    smuids.append(dtid)
                            if "slots" in conf:
                                if "list" in conf["slots"]:
                                    mux_rows = ()  # keeps track of what smu is connected to what slot/pad and their order
                                    for slot in conf["slots"]["list"]:
                                        to_upsert = {}  # a slot
                                        to_upsert["setup_id"] = seid
                                        to_upsert["name"] = slot["name"]
                                        x, y = slot["offset"]
                                        if (x is not None) and (y is not None):  # handle nulls
                                            to_upsert["place"] = slg.Point((x, y))
                                        else:
                                            to_upsert["place"] = None
                                        slid = self.upsert("tbl_setup_slots", to_upsert)  # the slots will go in in order
                                        assert slid > 0, "setup slot registration fail"
                                        if "group_order" in conf["slots"]:
                                            for order_major, group in enumerate(conf["slots"]["group_order"]):
                                                for order_minor, element in enumerate(group):
                                                    this_slot = element[0]
                                                    pad = element[1]
                                                    this_smuid = smuids[order_minor]
                                                    if slot["name"] == this_slot:
                                                        mux_rows += ((order_major, order_minor, slid, pad, this_smuid),)
                                    mr_hash = hash(mux_rows) & 0x7FFFFFFF  # compute the hash of the mux row setup
                                    mx_setup_id = self.upsert("tbl_mux_set_hashes", {"hash": mr_hash}, expect_mod=True)
                                    if mx_setup_id < 1:  # insert collision
                                        mx_setup_id = self.get("tbl_mux_set_hashes", ["id"], condition=f"hash = {mr_hash}")[0][0]
                                        assert mx_setup_id > 0, "mux setup id fetch fail"
                                    else:  # new set
                                        for mux_row in mux_rows:
                                            to_upsert = {}
                                            to_upsert["set_hash_id"] = mx_setup_id
                                            to_upsert["order_major"] = mux_row[0]
                                            to_upsert["order_minor"] = mux_row[1]
                                            to_upsert["slot_id"] = mux_row[2]
                                            to_upsert["pad"] = mux_row[3]
                                            to_upsert["tool_id"] = mux_row[4]
                                            mrid = self.upsert("tbl_mux", to_upsert)
                                            assert mrid > 0, "mux set row registration fail"
                                    caid = self.upsert("tbl_conf_as", {"mux_set_hash_id": mx_setup_id}, id=id)
                                    assert caid == id, "mux set hash id registration fail"
                if "substrates" in conf:
                    if "layouts" in conf["substrates"]:
                        for layout in conf["substrates"]["layouts"]:
                            to_upsert = {}  # layouts
                            to_upsert["name"] = layout["name"]
                            if "version" in layout:
                                to_upsert["version"] = layout["version"]
                            if layout["size"]:  # catches empty ones
                                x = layout["size"][0]
                                y = layout["size"][1]
                                if (x is not None) and (y is not None):  # handle nulls
                                    to_upsert["extents"] = slg.Box((x / 2, y / 2), (-x / 2, -y / 2))
                            lid = self.upsert("tbl_layouts", to_upsert)
                            assert lid > 0, "layout registration fail"
                            for i, pad_no in enumerate(layout["pads"]):
                                location = layout["locations"][i]
                                area = layout["areas"][i]
                                dark_area = layout["dark_areas"][i]
                                to_upsert = {}  # layout_devices
                                to_upsert["layout_id"] = lid
                                to_upsert["pad_no"] = pad_no
                                if area > 0:  # catch -1 area entry
                                    to_upsert["light_cir"] = slg.Circle((location[0], location[1]), float(math.sqrt(area / math.pi)))
                                    to_upsert["dark_cir"] = slg.Circle((location[0], location[1]), float(math.sqrt(dark_area / math.pi)))
                                # to_upsert["light_path"] = lid
                                # to_upsert["dark_path"] = lid
                                did = self.upsert("tbl_layout_devices", to_upsert)
                                assert did > 0, "layout device registration fail"
                    if "kinds" in conf["substrates"]:
                        for skind in conf["substrates"]["kinds"]:
                            to_upsert = {}  # skind kind
                            to_upsert["mfg"] = skind["mfg"]
                            to_upsert["tco_type"] = skind["tco_type"]
                            to_upsert["nominal_rs"] = skind["nominal_rs"]
                            to_upsert["batch"] = skind["batch"]
                            to_upsert["pattern_name"] = skind["pattern_name"]
                            to_upsert["sheet_resistance"] = skind["sheet_resistance"]
                            to_upsert["t_550"] = skind["t_550"]
                            lokid = self.upsert("tbl_substrate_types", to_upsert)
                            assert lokid > 0, "layout kind registration fail"
            elif kind == "b":
                # TODO remove user, runname and timestamp info from conf b
                # discard epoch counter from gui altogether. also remove all substrate table data from b
                # and split that all out into conf type c (b will then be often duplicated and c will be largely unique per run)
                self.register_user(conf["user_name"])
        return id

    def register_run(self, uid: int, conf_a_id: int, conf_b_id: int, backend_ver: str, name: str = "") -> int:
        to_upsert = {}
        to_upsert["user_id"] = uid
        to_upsert["conf_a_id"] = conf_a_id
        to_upsert["conf_b_id"] = conf_b_id
        to_upsert["name"] = name
        to_upsert["dbsw_ver"] = importlib.metadata.version("slothdb")
        to_upsert["backend_ver"] = backend_ver

        return self.upsert(f"tbl_runs", to_upsert, expect_mod=True)

    def new_light_cal(
        self,
        sid: int,
        rid: int | None = None,
        temps: tuple[float] | None = None,
        raw_int: float | None = None,
        raw_spec: list[tuple[float]] | None = None,
        raw_int_spec: float | None = None,
        setpoint: tuple[float] | None = None,
        recipe: str | None = None,
        idn: str | None = None,
    ) -> int:
        to_upsert = {}
        to_upsert["setup_id"] = sid
        if rid:
            to_upsert["run_id"] = rid
        if temps:
            to_upsert["temperature"] = list(temps)
        to_upsert["raw_intensity"] = raw_int
        if raw_spec:
            to_upsert["raw_spectrum"] = [list(x) for x in raw_spec]
        if raw_int_spec:
            to_upsert["raw_int_spec"] = raw_int_spec
        if setpoint:
            to_upsert["setpoint"] = list(setpoint)
        if recipe:
            to_upsert["recipe"] = recipe
        if idn:
            to_upsert["idn"] = idn
        return self.upsert(f"tbl_light_cal", to_upsert)

    def complete_event(self, eid: int, kind: en.Event, complete=True) -> int:
        """updates a master event's completeness"""
        if kind == en.Event.NONE:
            return -5
        else:
            return self.upsert(f"tbl_{kind.value}_events", {"complete": complete}, id=eid)

    def complete_run(self, rid: int, complete: bool = True) -> int:
        """updates a run entry's completeness"""
        if complete:
            self.pg_cmd([f"ALTER SEQUENCE IF EXISTS rds RESTART"])
        return self.upsert(f"{self.schemadot}tbl_runs", {"complete": complete}, id=rid)

    def new_ss_event(self, rid: int, did: int, fixed: en.Fixed = en.Fixed.VOLTAGE, setpoint: float = 0.0, ea: float | None = None, complete: bool = False):
        """registeres a new steady state event"""
        to_upsert = {}
        to_upsert["run_id"] = rid
        to_upsert["device_id"] = did
        to_upsert["complete"] = complete
        to_upsert["fixed"] = fixed
        to_upsert["setpoint"] = setpoint
        to_upsert["effective_area"] = ea
        return self.upsert(f"{self.schemadot}tbl_ss_events", to_upsert)

    @contextlib.contextmanager
    def listen_notify_ctx(self, channels: list[str]):
        """contect manager to manage sending LISTEN and UNLISTEN commands to subscribe and unsubscribe from database notifications"""
        # self.connection.execute(f"SET search_path TO '$user',{self.schema},{self.schema}_raw")
        # self.connection.commit()
        for channel in channels:
            self.pg_cmd([f"LISTEN {channel}"])
        try:
            yield None
        finally:
            for channel in channels:
                self.pg_cmd([f"UNLISTEN {channel}"])

    @staticmethod
    def register_adapters(cursor: psycopg.Cursor):
        """registers my geometry adapters https://github.com/psycopg/psycopg/issues/392"""
        cursor.adapters.register_dumper(slg.Point, slg.PointDumper)
        cursor.adapters.register_loader("point", slg.PointLoader)
        cursor.adapters.register_dumper(slg.Line, slg.LineDumper)
        cursor.adapters.register_loader("line", slg.LineLoader)
        cursor.adapters.register_dumper(slg.LineSegment, slg.LineSegmentDumper)
        cursor.adapters.register_loader("lseg", slg.LineSegmentLoader)
        cursor.adapters.register_dumper(slg.Box, slg.BoxDumper)
        cursor.adapters.register_loader("box", slg.BoxLoader)
        cursor.adapters.register_dumper(slg.Path, slg.PathDumper)
        cursor.adapters.register_loader("path", slg.PathLoader)
        cursor.adapters.register_dumper(slg.Polygon, slg.PolygonDumper)
        cursor.adapters.register_loader("polygon", slg.PolygonLoader)
        cursor.adapters.register_dumper(slg.Circle, slg.CircleDumper)
        cursor.adapters.register_loader("circle", slg.CircleLoader)

    def upsert(self, table: str, to_upsert: dict, id: int | None = None, expect_mod: bool = False) -> int:
        """
        inserts/updates a single row.
        - if id is given, it's an update to an existing row of that id
        - if no id is given, it's an insert of a new row, unless there's a conflict,
        then find the conflicting row and try to update the non-conflicting cols
        - returns the id of the modified row
        """
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
                        SlothDBSync.register_adapters(cur)  # TODO: can/should this be done at a higher level?
                        table = table.removeprefix(self.schemadot)
                        tables = f"{self.schemadot}{table}"
                        if id:  # update
                            mod_pairs = ", ".join([" = ".join((f"{key}", f"%({key})s")) for key in to_upsert.keys()])
                            cur.execute(f"UPDATE {tables} SET {mod_pairs} WHERE id = {id};", to_upsert)
                            ret = id
                        else:  # insert
                            cols = ", ".join([str(key) for key in to_upsert.keys()])
                            cols_vals = ", ".join([f"%({key})s" for key in to_upsert.keys()])
                            if (not cols) or (not cols_vals):  # handle blank insert case
                                cur.execute(f"INSERT INTO {tables} DEFAULT VALUES ON CONFLICT DO NOTHING RETURNING id")
                            else:
                                cur.execute(f"INSERT INTO {tables}({cols}) VALUES ({cols_vals}) ON CONFLICT DO NOTHING RETURNING id", to_upsert)
                            update_cols = None
                            # test for conflict on insertion attempt
                            if (cur.statusmessage) and (cur.statusmessage.upper().startswith("INSERT 0 0")):  # "INSERT 0 0" means nothing was inserted
                                if expect_mod:
                                    return -4  # user wants modification, but db constraint prevents it
                                # figure out what could have caused the collision (find the cols here with some sort of constraint)
                                ucols_q = f"""
                                select
                                    t.column_name,
                                    t.column_default,
                                    generation_expression
                                from
                                    information_schema.columns as t
                                join information_schema.constraint_column_usage as cu
                                        using (table_catalog,
                                    table_schema,
                                    table_name,
                                    column_name)
                                join information_schema.table_constraints as c
                                        using (table_catalog,
                                    table_schema,
                                    table_name,
                                    constraint_name)
                                where
                                    table_schema = '{self.schema}'
                                    and table_name = '{table}'
                                """
                                cur.execute(ucols_q)
                                ccolsf = cur.fetchall()
                                ccols = [x[0] for x in ccolsf]  # these are the cols that have a constraint
                                defs = [x[1] for x in ccolsf]  # these are the default values for those
                                gens = [x[2] for x in ccolsf]  # these are all the generation expressions for those cols
                                for gen in gens:  # loop through the generation expressions
                                    if gen:
                                        for upsert_col in to_upsert.keys():
                                            if upsert_col in gen:
                                                # if we find an upsert col name in a generation expression,
                                                # add it to the list of constrained cols (it's really constrained by association)
                                                ccols.append(upsert_col)
                                collision_cols = set(to_upsert.keys()).intersection(ccols)  # col(s) that collided
                                update_cols = set(to_upsert.keys()).difference(ccols)  # col(s) that didn't collide
                                if not collision_cols:  # nothing obviously collided, but we still did, so the collision must involve default values
                                    for i, thisdef in enumerate(defs):
                                        if thisdef:
                                            col = ccols[i]
                                            collision_cols.add(col)
                                            type_split = thisdef.rsplit("::", 1)  # get rid of the type
                                            to_upsert[col] = type_split[0].strip("'")
                                # now use the colliding cols to find the row we just collided with
                                constraint_pairs = zip([str(key) for key in collision_cols], [f"%({key})s" for key in collision_cols])
                                constraints = " AND ".join([f"{pair[0]} = {pair[1]}" for pair in constraint_pairs])
                                cur.execute(f"SELECT id FROM {tables} WHERE {constraints};", to_upsert)
                                if cur.statusmessage and cur.statusmessage.upper() == "SELECT 0":
                                    return -5  # we couldn't find the conflicting row
                            fetchd = cur.fetchone()  # returns the id of the row of ineterest here
                            if fetchd:
                                ret = fetchd[0]
                                if update_cols:  # do update of existing row only if there is one to do
                                    mod_pairs = ", ".join([" = ".join((f"{key}", f"%({key})s")) for key in update_cols])
                                    # TODO: consider trying to avoid resetting the same value
                                    cur.execute(f"UPDATE {tables} SET {mod_pairs} WHERE id = {ret}", to_upsert)
                            else:
                                ret = -3  # unable to fetch
            except Exception as e:
                self.lg.debug(f"Problem upserting: {repr(e)}")
                ret = -1  # general exception
        else:
            ret = -2  # not connected
        return ret

    def get(self, table: str, colfilter: Iterable = ("*",), condition: str = "") -> Any:
        """get data from a table
        condition is fed to the WHERE row filter
        specify a column name or list of them to get just that/those,
        otherwise you get the whole row"""
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
                        SlothDBSync.register_adapters(cur)
                        if self.schemadot and (not table.startswith(self.schemadot)):
                            table = f"{self.schemadot}{table}"
                        if condition:
                            condition = f" WHERE {condition}"
                        cur.execute(f"SELECT {', '.join(colfilter)} from {table}{condition}")
                        ret = cur.fetchall()
            except Exception as e:
                self.lg.debug(f"Problem getting: {repr(e)}")
                ret = None
        else:  # not connected
            ret = None
        return ret

    def delete(self, table: str, condition: str) -> int:
        """deletes all frows matching a condition from a table
        example usage: db.delete("tbl_users", "id = 4")  # deletes user with id == 4
        """
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
                        if self.schemadot and (not table.startswith(self.schemadot)):
                            table = f"{self.schemadot}{table}"
                        if condition:
                            cur.execute(f"DELETE FROM {table} WHERE {condition}")
                        else:  # delete all rows if there's no condition
                            cur.execute(f"DELETE FROM {table}")
                        del_msg = cur.statusmessage  # how many rows did we just delete?
            except Exception as e:
                self.lg.debug(f"Problem upserting: {repr(e)}")
                ret = -1
            else:
                if del_msg and del_msg.startswith("DELETE "):
                    ret = int(del_msg.lstrip("DELETE "))  # return the number of rows deleted
                else:
                    self.lg.debug(f"Tried to delete rows from {table} but {del_msg} instead")
                    ret = -3
        else:
            ret = -2
        return ret

    def vac(self, tables: tuple = ()):
        """database VACUUMer"""
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    cur.execute(f'VACUUM ANALYZE {", ".join(tables)}')
            except Exception as e:
                self.lg.debug(f"Problem vacuuming: {repr(e)}")
                ret = -1
            else:
                ret = 0
        else:
            ret = -2
        return ret

    def setup_tbls(self, recreate_tables: bool = False, verbose_notify: bool = False, redo_notify: bool = False) -> tuple[int, int]:
        """(re)initialize the tables, setup notifications, causes complete data loss if recreate_tables=True"""
        tables = []  # holds instructions for how to build tables, order is important

        # keeps track of all the layouts we have
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_layouts"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("name text NOT NULL")  # layout name
        col_setup_strs.append("version text")  # semver compatible string or null telling us what revision this is TODO: make this NULL NOT UNIQUE in version 15
        col_setup_strs.append("extents box")  # the extents of the substrate in mm, centered around 0,0
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        setup["links"] = links
        # TODO: NULL IS NOT UNIQUE in 15 for version col
        setup["unique"] = [("name", "version")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # keeps track of every device design we have
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_layout_devices"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("layout_id int NOT NULL")  # what layout is this device in?
        col_setup_strs.append("pad_no int NOT NULL")  # pad number for this device
        col_setup_strs.append("light_cir circle")  # a circle representing the light area
        col_setup_strs.append("dark_cir circle")  # a circle representing the dark area
        col_setup_strs.append("light_path path")  # a path representing the outline shape of the light area
        col_setup_strs.append("dark_path path")  # a path representing the outline shape of the dark area
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "layout_id", "other_tbl": f"{self.schemadot}tbl_layouts", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("layout_id", "pad_no")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # keeps track of all the substrate types we know about
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_substrate_types"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when we came to know about this substrate type
        col_setup_strs.append("mfg text NOT NULL DEFAULT ''")  # manufacturer/seller of the substrate
        col_setup_strs.append("tco_type text NOT NULL DEFAULT ''")  # ITO, FTO etc
        col_setup_strs.append("nominal_rs text NOT NULL DEFAULT ''")  # what the mfg claims the Rs to be (10 ohm/sq, TEC7, etc.)
        col_setup_strs.append("batch text NOT NULL DEFAULT ''")  # tells us what order the tco came in with
        col_setup_strs.append("pattern_name text NOT NULL DEFAULT ''")  # horse.dxf etc
        col_setup_strs.append("sheet_resistance real")  # measured sheet resistance
        col_setup_strs.append("t_550 real")  # percent of 550nm light that passes through the substrate
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        setup["links"] = links
        setup["unique"] = [("mfg", "tco_type", "nominal_rs", "batch", "pattern_name")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # table keeping track of all the labeled pieces (typically of glass) that we we know about
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_substrates"
        col_setup_strs = []
        col_setup_strs.append("id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when we came to know about this label
        col_setup_strs.append("name text")  # the label string
        col_setup_strs.append("layout_id int NOT NULL")  # what layout is used on this substrate?
        col_setup_strs.append("type_id int")  # the type of substrate labeled here TODO: make NOT NULL when we can link this properly
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "type_id", "other_tbl": f"{self.schemadot}tbl_substrate_types", "other_col": "id"})
        links.append({"this_col": "layout_id", "other_tbl": f"{self.schemadot}tbl_layouts", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("name", "layout_id")]  # iterable of col names that will be enforced to to be unique
        # TODO: change the unique enforcement to just name here when we can enforce/alert the user properly that there's a name clash
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # table keeping track of all the devices, one per individual device fabricated
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_devices"
        col_setup_strs = []
        col_setup_strs.append("id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("substrate_id int NOT NULL")  # the substrate this pixel is on
        col_setup_strs.append("layout_device_id int NOT NULL")  # the device design employed here
        col_setup_strs.append("type int NOT NULL DEFAULT 1")  # dev_type enum, (solar cell or led)
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "substrate_id", "other_tbl": f"{self.schemadot}tbl_substrates", "other_col": "id"})
        links.append({"this_col": "layout_device_id", "other_tbl": f"{self.schemadot}tbl_layout_devices", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("substrate_id", "layout_device_id")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # sites
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_sites"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("name text NOT NULL")  # the name of a site
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        setup["links"] = links
        setup["unique"] = [("name",)]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # measurement setups
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_setups"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("name text NOT NULL")  # the name of the measurement setup
        col_setup_strs.append("site_id int NOT NULL")  # description of where the setup is located
        col_setup_strs.append("experiments jsonb")  # flexible json field that holds details for different experiments the setup supports
        # currently I can only think of keeping stage centers here, so an entry would be like: {"eqe":{"center":(23.4, 286.1)}, "solarsim":{"center":(413.9, 286.1)}}
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "site_id", "other_tbl": f"{self.schemadot}tbl_sites", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("name", "site_id")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # measurement slots, one for each thing that can hold a substrate in a setup
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_setup_slots"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("setup_id int NOT NULL")  # links to the setup table, tells us which setup this slot is for
        col_setup_strs.append("name text NOT NULL")  # the name/designator of the slot A, A1, etc
        col_setup_strs.append("place point")  # offset from origin of the slot's center
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "setup_id", "other_tbl": f"{self.schemadot}tbl_setups", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("name", "setup_id")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = ()  # col names to partition the table by
        tables.append(setup)

        # users table
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_users"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when the row was created
        col_setup_strs.append("name text NOT NULL")  # login
        col_setup_strs.append("email text")
        col_setup_strs.append("token bytea")  # some token for validation (like a hashed pw for example)
        col_setup_strs.append("kind smallint NOT NULL")  # user type enum
        col_setup_strs.append("active bool DEFAULT true")  # user type enum
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        setup["links"] = links
        setup["unique"] = [("name",)]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # data collection tools table (smus for example)
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_tools"  # holds details of the smus and other data collection tools
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("setup_id int NOT NULL")  # links to the setup table, tells us which setup this slot is for
        col_setup_strs.append("address text NOT NULL")  # comms address for the smu
        col_setup_strs.append("idn text")  # identification string returned by the tool
        col_setup_strs.append("kind smallint NOT NULL DEFAULT 1")  # DataTools enum
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "setup_id", "other_tbl": f"{self.schemadot}tbl_setups", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("setup_id", "address")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # table for listing hashes for sets of slot-tool pairs
        # hashes made from python like: hash = hash(frozenset(((slot_id, tool_id),(slot_id, tool_id)))) & 0x7FFFFFFF
        # (example for pairings of just two slots)
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_mux_set_hashes"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")  # a counter for the number of unique sets of slot-tool mappings we have
        col_setup_strs.append("hash int NOT NULL")  # the hash of a mux table set
        # python example: hash = hash((((order, slot_id, pad, tool_id), (order, slot_id, pad, tool_id)))) & 0x7FFFFFFF
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        setup["links"] = links
        setup["unique"] = [("hash",)]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # table that keeps track of sets of mux cmd substrate, pad and smu mapping. with measurement order
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_mux"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("set_hash_id int NOT NULL")  # the id of hash of a set of mappings
        col_setup_strs.append("order_major int NOT NULL")  # a number that gives the group order
        col_setup_strs.append("order_minor int NOT NULL")  # a number that gives the group item order
        col_setup_strs.append("slot_id int NOT NULL")  # the id of a single slot
        col_setup_strs.append("pad int NOT NULL")  # the pad number
        col_setup_strs.append("tool_id int NOT NULL")  # the id of a single tool
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "set_hash_id", "other_tbl": f"{self.schemadot}tbl_mux_set_hashes", "other_col": "id"})
        links.append({"this_col": "tool_id", "other_tbl": f"{self.schemadot}tbl_tools", "other_col": "id"})
        links.append({"this_col": "slot_id", "other_tbl": f"{self.schemadot}tbl_setup_slots", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("set_hash_id", "slot_id", "pad", "tool_id")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # table for storing measurement config type As (configuration that tends to change less often)
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_conf_as"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY")
        col_setup_strs.append("hash char(32) GENERATED ALWAYS AS (md5(conf::text)) STORED")  # the hash of the config json
        col_setup_strs.append("note text")  # a note that a user can add to for this config
        col_setup_strs.append("mux_set_hash_id int")  # the mux setup hash used here (redundant with info in this config) TODO: make NOT NULL
        col_setup_strs.append("conf jsonb NOT NULL")  # the type A config
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "mux_set_hash_id", "other_tbl": f"{self.schemadot}tbl_mux_set_hashes", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("hash",)]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # table for storing measurement config type Bs (configuration that tends to change more often)
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_conf_bs"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("hash char(32) GENERATED ALWAYS AS (md5(conf::text)) STORED")  # the hash of the config json
        col_setup_strs.append("note text")  # a note that a user can add to for this config
        col_setup_strs.append("conf jsonb NOT NULL")  # the type B config
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        setup["links"] = links
        setup["unique"] = [("hash",)]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # runs table
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_runs"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when the row was created
        col_setup_strs.append("name text")  # user entered name for the run
        # col_setup_strs.append("frontend_ver text NOT NULL")  # version string of the backend TODO
        col_setup_strs.append("backend_ver text NOT NULL")  # version string of the backend
        # col_setup_strs.append("backend_ver text")  # version string of the backend TODO
        col_setup_strs.append("dbsw_ver text NOT NULL")  # version string of the database software
        col_setup_strs.append("user_id int NOT NULL")  # the id for the user that started the run
        # col_setup_strs.append("user_id int")  # the id for the user that started the run TODO
        col_setup_strs.append("conf_a_id int NOT NULL")  # the id for the type a config used here, for less often changes
        col_setup_strs.append("conf_b_id int NOT NULL")  # the id for the type b config used here, for more often changes
        # col_setup_strs.append("conf_c_id int NOT NULL")  # the id for the type c config used here, for substrate table TODO
        col_setup_strs.append("complete bool NOT NULL DEFAULT false")  # is the run complete?
        # col_setup_strs.append("started bool NOT NULL DEFAULT false")  # has the run satrted? TODO
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = "json_build_object('id',NEW.id,'complete',NEW.complete)::text"
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "user_id", "other_tbl": f"{self.schemadot}tbl_users", "other_col": "id"})
        links.append({"this_col": "conf_a_id", "other_tbl": f"{self.schemadot}tbl_conf_as", "other_col": "id"})
        links.append({"this_col": "conf_b_id", "other_tbl": f"{self.schemadot}tbl_conf_bs", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = []  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # run-active devices table, records which devices were selected for measurement in each run
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_run_devices"
        col_setup_strs = []
        col_setup_strs.append("run_id int NOT NULL")
        col_setup_strs.append("device_id bigint NOT NULL")
        col_setup_strs.append("PRIMARY KEY (run_id, device_id)")  # the primary key here is a composit
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "device_id", "other_tbl": f"{self.schemadot}tbl_devices", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = []  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # table that keeps track of how substrates were loaded into slots for a run
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_slot_substrate_run_mappings"
        col_setup_strs = []
        col_setup_strs.append("id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("run_id int NOT NULL")  # the id of a run
        col_setup_strs.append("slot_id int NOT NULL")  # the id of a slot
        col_setup_strs.append("substrate_id bigint NOT NULL")  # the id of a substrate
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "substrate_id", "other_tbl": f"{self.schemadot}tbl_substrates", "other_col": "id"})
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "slot_id", "other_tbl": f"{self.schemadot}tbl_setup_slots", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("slot_id", "run_id", "substrate_id")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # steady state event table
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_ss_events"
        col_setup_strs = []
        col_setup_strs.append("id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY")  # steady state event counter
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when the event began
        col_setup_strs.append("run_id int NOT NULL")  # run id for this event
        col_setup_strs.append("ecs int NOT NULL")  # the relative temporal order for this experiment in this run for this device
        col_setup_strs.append("device_id int NOT NULL")  # id of a specific device
        col_setup_strs.append("complete bool DEFAULT false")  # is the event's data entry complete?
        col_setup_strs.append("fixed smallint")  # the thing that's being controlled by the external circuit: enum voltage, current, resistance
        col_setup_strs.append("setpoint real")  # the setpoint for the thing that's controlled
        col_setup_strs.append("effective_area real")  # the area (in m^2) that we should use for calculating things like current density here, null means use standard area
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        # setup["custom_notify_function_payload"] = "json_build_object('id',NEW.id,'complete',NEW.complete)::text"
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = True  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "device_id", "other_tbl": f"{self.schemadot}tbl_devices", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("run_id", "device_id", "ecs")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # light intensity sweep event table (probably just suns-voc)
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_isweep_events"
        col_setup_strs = []
        col_setup_strs.append("id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY")  # steady state event counter
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when the event began
        col_setup_strs.append("run_id int NOT NULL")  # run id for this event
        col_setup_strs.append("ecs int NOT NULL")  # the relative temporal order for this experiment in this run for this device
        col_setup_strs.append("device_id int NOT NULL")  # id of a specific device
        col_setup_strs.append("complete bool DEFAULT false")  # is the event's data entry complete?
        col_setup_strs.append("fixed smallint")  # the thing that's being controlled by the external circuit: enum voltage, current, resistance
        col_setup_strs.append("setpoint real")  # the setpoint for the thing that's controlled
        col_setup_strs.append("linear bool DEFAULT false")  # is the intensity sweep linear?
        col_setup_strs.append("n_points integer")  # number of sweep steps for linear sweep
        col_setup_strs.append("from_isetpoint real")  # first intensity setpoint for a linear sweep
        col_setup_strs.append("to_isetpoint real")  # last intensity setpoint for a linear sweep
        col_setup_strs.append("isetpoints real[]")  # the intensity setpoints in percent of 1 sun nominal
        col_setup_strs.append("effective_area real")  # the area (in m^2) that we should use for calculating things like current density here
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        # setup["custom_notify_function_payload"] = "json_build_object('id',NEW.id,'complete',NEW.complete)::text"
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = True  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "device_id", "other_tbl": f"{self.schemadot}tbl_devices", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("run_id", "device_id", "ecs")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # electronic sweep event table
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_sweep_events"
        col_setup_strs = []
        col_setup_strs.append("id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY")  # steady state event counter
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when the event began
        col_setup_strs.append("run_id int NOT NULL")  # run id for this event
        col_setup_strs.append("ecs int NOT NULL")  # the relative temporal order for this experiment in this run for this device
        col_setup_strs.append("device_id int NOT NULL")  # id of a specific device
        col_setup_strs.append("complete bool DEFAULT false")  # is the event's data entry complete?
        col_setup_strs.append("fixed smallint")  # the thing that's being controlled by the external circuit: enum voltage, current, resistance
        col_setup_strs.append("linear bool DEFAULT true")  # is the intensity sweep linear?
        col_setup_strs.append("n_points integer")  # number of sweep steps for linear sweep
        col_setup_strs.append("from_setpoint real")  # first fixed setpoint for a linear sweep
        col_setup_strs.append("to_setpoint real")  # last fixed setpoint for a linear sweep
        col_setup_strs.append("setpoints real[]")  # a list of the the setpoint values
        col_setup_strs.append("effective_area real")  # the area (in m^2) that we should use for calculating things like current density here
        col_setup_strs.append("light bool DEFAULT true")  # was the light on during this sweep?
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        # setup["custom_notify_function_payload"] = "json_build_object('id',NEW.id,'complete',NEW.complete)::text"
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = True  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "device_id", "other_tbl": f"{self.schemadot}tbl_devices", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("run_id", "device_id", "ecs")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # mppt event table
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_mppt_events"
        col_setup_strs = []
        col_setup_strs.append("id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY")  # steady state event counter
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when the event began
        col_setup_strs.append("run_id int NOT NULL")  # run id for this event
        col_setup_strs.append("ecs int NOT NULL")  # the relative temporal order for this experiment in this run for this device
        col_setup_strs.append("device_id int NOT NULL")  # id of a specific device
        col_setup_strs.append("complete bool DEFAULT false")  # is the event's data entry complete?
        col_setup_strs.append("algorithm text")  # first intensity setpoint for a linear sweep
        col_setup_strs.append("effective_area real")  # the area (in m^2) that we should use for calculating things like current density here
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        # setup["custom_notify_function_payload"] = "json_build_object('id',NEW.id,'complete',NEW.complete)::text"
        setup["custom_notify_function_payload"] = ""
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = True  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "device_id", "other_tbl": f"{self.schemadot}tbl_devices", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = [("run_id", "device_id", "ecs")]  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # light source calibration table
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_light_cal"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when this row was created
        col_setup_strs.append("run_id int")  # run id for this event (optional)
        col_setup_strs.append("setup_id int NOT NULL")  # an id from the setup table
        col_setup_strs.append("idn text")  # identification string returned by instrument
        col_setup_strs.append("recipe text")  # name of the light engine recipe
        col_setup_strs.append("temperature real[]")  # list of temperature measurements from the light source
        col_setup_strs.append("setpoint real[]")  # the setpoint(s) of the light source (in its favorite unit, maybe percent?), multiple values to handle direct channel programming
        col_setup_strs.append("raw_intensity real")  # raw measured intensity magnitude number (maybe as measured by a photodiode)
        col_setup_strs.append("raw_spectrum real[][2]")  # raw spectrum measurement n rows of (wavelangth, intesnity)
        col_setup_strs.append("raw_int_spec real")  # intensity number found by integrating the spectrum data
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""  # " || ".join(["NEW.event_id::text"])
        # setup["custom_notify_function_payload"] = "json_build_object('id',NEW.id,'kind',NEW.kind,'complete',NEW.complete)::text"
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "setup_id", "other_tbl": f"{self.schemadot}tbl_setups", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = []  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # contact resistance table
        setup = {}
        setup["schema"] = self.schema
        setup["name"] = "tbl_contact_checks"
        col_setup_strs = []
        col_setup_strs.append("id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY")
        col_setup_strs.append("ts timestamptz DEFAULT current_timestamp")  # when this row was created
        col_setup_strs.append("run_id int")  # run id for this event (optional)
        col_setup_strs.append("substrate_id bigint NOT NULL")  # an id from the substrate table
        col_setup_strs.append("setup_slot_id int NOT NULL")  # an id from the setup_slot table
        col_setup_strs.append("pad_name text NOT NULL")  # the name of the pad being checked
        col_setup_strs.append("pass bool NOT NULL")  # if the contact check passed
        col_setup_strs.append("r real NOT NULL")  # resistance value measured
        setup["col_setup_str"] = ", ".join(col_setup_strs)
        setup["custom_notify_function_payload"] = ""  # " || ".join(["NEW.event_id::text"])
        # setup["custom_notify_function_payload"] = "json_build_object('id',NEW.id,'kind',NEW.kind,'complete',NEW.complete)::text"
        setup["notify_id"] = False  # notification that just sends up the changed row's id
        setup["notify_big"] = False  # notification that sends up the entire changed row
        links = []
        links.append({"this_col": "run_id", "other_tbl": f"{self.schemadot}tbl_runs", "other_col": "id"})
        links.append({"this_col": "substrate_id", "other_tbl": f"{self.schemadot}tbl_substrates", "other_col": "id"})
        links.append({"this_col": "setup_slot_id", "other_tbl": f"{self.schemadot}tbl_setup_slots", "other_col": "id"})
        setup["links"] = links
        setup["unique"] = []  # iterable of col names that will be enforced to to be unique
        setup["index"] = []  # set up indexing to (hopefully) improve query speed
        setup["partition"] = []  # col names to partition the table by
        tables.append(setup)

        # raw data table template
        raw_setup = {}
        raw_setup["schema"] = self.schema
        raw_setup["name"] = "placeholder"  # overwritten below
        col_setup_strs = []
        col_setup_strs.append("eid bigint NOT NULL")  # what event is this data for?
        col_setup_strs.append("dcs int NOT NULL")  # data counter sequence number. must be overwritten by a local session sequence
        # col_setup_strs.append("dcs int NOT NULL DEFAULT nextval('dat_seq')")  # data counter sequence number. must be overwritten by a local session sequence
        col_setup_strs.append("v real")  # sensed voltage [V]
        col_setup_strs.append("i real")  # sensed current [A]
        col_setup_strs.append("t real")  # hardware timestamp [s]
        col_setup_strs.append("s integer")  # hardware status
        col_setup_strs.append("PRIMARY KEY (eid, dcs)")  # the primary key here is a composit
        raw_setup["col_setup_str"] = ", ".join(col_setup_strs)
        raw_setup["custom_notify_function_payload"] = ""  # " || ".join(["NEW.event_id::text"])
        raw_setup["notify_id"] = False  # notification that just sends up the changed row's id
        raw_setup["notify_big"] = True  # notification that sends up the entire changed row
        links = []
        # links.append({"this_col": "event_id", "other_tbl": f"{self.schemadot}tbl_events", "other_col": "id"})
        raw_setup["links"] = links
        raw_setup["unique"] = []  # iterable of col names that will be enforced to to be unique
        raw_setup["index"] = []  # set up indexing to (hopefully) improve query speed
        raw_setup["partition"] = ()  # col names to partition the table by

        # setup the raw data table creation
        # fill the template
        raw_tables = []
        for table in tables:  # look through the tables for the _events ones and make a raw one for each of those
            name = table["name"]
            if "_events" in name:
                this_raw = raw_setup.copy()
                event = name.removeprefix("tbl_").removesuffix("_events")
                raw_name = f"tbl_{event}_raw"
                this_raw["name"] = raw_name
                this_raw["event"] = event
                this_raw["links"] = [{"this_col": "eid", "other_tbl": f"{self.schemadot}tbl_{event}_events", "other_col": "id"}]
                raw_tables.append(this_raw)

        raw_site_index = 1  # make the raw data tables now for the first site id that well have, 1
        tbls_ret = self.make_tbls(tables, recreate_tables=recreate_tables, redo_notify=redo_notify)
        raw_tbls_ret = self.make_tbls(raw_tables, recreate_tables=False, redo_notify=False, raw_site_index=raw_site_index)
        return (tbls_ret, raw_tbls_ret)

    def make_tbls(self, tables: list, recreate_tables: bool = False, redo_notify: bool = False, raw_site_index: int = 0) -> int:
        """creates tables in the DB"""
        if hasattr(self, "connection") and not self.connection.closed:
            try:
                with self.connection.cursor() as cur:
                    with self.connection.transaction(force_rollback=self.force_rollback):  # for atomic autocommits
                        if recreate_tables:
                            self.lg.warning(f"Destroying all metadata data.")
                            cur.execute("DROP PUBLICATION IF EXISTS full_pub")  # publication for everything
                            cur.execute("CREATE PUBLICATION full_pub FOR ALL TABLES")
                            cur.execute("DROP PUBLICATION IF EXISTS non_raw_pub")  # publication for everything except raw data
                            cur.execute("CREATE PUBLICATION non_raw_pub")

                            if self.schema != "":
                                cur.execute(f"DROP SCHEMA IF EXISTS {self.schema} CASCADE")  # delete base schema
                                self.lg.warning(f"Tables destroyed successfully.")
                            else:
                                cur.execute("DROP SCHEMA public CASCADE;")
                                cur.execute("CREATE SCHEMA public;")
                                cur.execute("GRANT ALL ON SCHEMA public TO postgres;")
                                cur.execute("GRANT ALL ON SCHEMA public TO public;")
                                cur.execute("COMMENT ON SCHEMA public IS 'standard public schema'")

                            # this dummy sequence will be created schemaless because it must be overwritten in by each local raw data-creating session
                            # cur.execute(f"DROP SEQUENCE IF EXISTS dat_seq")  # new dummy data counter sequence
                            # designed to give an error if you try to use it, must be overwritten in every local session that creates raw data
                            # cur.execute(f"CREATE SEQUENCE IF NOT EXISTS dat_seq MINVALUE 0 MAXVALUE 1 START WITH 1 NO CYCLE")  # new dummy data counter sequence
                            # the client can then make a temporary sequence for itself with the following:
                            # # spin up a new raw data counter sequence for this device
                            # #ret = db.pg_cmd([f"CREATE TEMPORARY SEQUENCE dat_seq MINVALUE 1 NO MAXVALUE START WITH 1 CYCLE"])  # overwrites the dummy sequence
                            # #assert ret >= 0, "Unable to start data counter"

                        if self.schema != "":
                            cur.execute(f"CREATE SCHEMA IF NOT EXISTS {self.schema}")  # new schema

                            if recreate_tables:
                                # TODO: will be allowed to use this with postgresql 15
                                # cur.execute(f"CREATE PUBLICATION non_raw_pub FOR ALL TABLES IN SCHEMA {self.schema}")
                                # cur.execute(f"CREATE PUBLICATION full_pub FOR ALL TABLES IN SCHEMA {self.schema}, {self.schema}_raw")
                                pass

                        for tbl in tables:
                            if raw_site_index == 0:
                                name = tbl["name"]
                            else:  # this is a raw data table, put a site index number in its name
                                name = f'{tbl["name"]}_{raw_site_index}'
                            if tbl["schema"] == "":
                                schema_dot = ""
                                schema_ul = ""
                            else:
                                schema_dot = f'{tbl["schema"]}.'
                                schema_ul = f'{tbl["schema"]}_'

                            create_str = f'CREATE TABLE IF NOT EXISTS {schema_dot}{name} ({tbl["col_setup_str"]})'
                            if tbl["partition"]:
                                create_str += f' PARTITION BY LIST ({", ".join(tbl["partition"])})'
                            cur.execute(f"{create_str}")

                            # setup foreign key references
                            for link in tbl["links"]:
                                from_tbl = link["other_tbl"]
                                from_col = link["other_col"]
                                to_col = link["this_col"]
                                cur.execute(f"ALTER TABLE {schema_dot}{name} ADD FOREIGN KEY ({to_col}) REFERENCES {from_tbl} ({from_col})")

                            # setup unique column enforcement (pairs of cols work too)
                            # TODO: add NULLS NOT UNIQUE to this when 15 comes out
                            for ucols in tbl["unique"]:
                                cur.execute(f'ALTER TABLE {schema_dot}{name} ADD UNIQUE ({", ".join(ucols)})')

                            # setup indexing
                            for idx in tbl["index"]:
                                cur.execute(f"DROP INDEX IF EXISTS {schema_ul}{name}_{idx[0]}")  # clean indexers
                                cur.execute(f'CREATE INDEX {schema_ul}{name}_{idx[0]} ON {schema_dot}{name} ({", ".join(idx)})')

                            # notification triggers and functions
                            if redo_notify:
                                # clean off triggers (this must be done first before the functions can be cleaned)
                                cur.execute(f"DROP TRIGGER IF EXISTS {name}_changed_id ON {schema_dot}{name}")
                                cur.execute(f"DROP TRIGGER IF EXISTS {name}_changed_big ON {schema_dot}{name}")
                                cur.execute(f"DROP TRIGGER IF EXISTS {name}_changed_custom ON {schema_dot}{name}")

                                # clean custom function (just these custom functions are okay to clean now because only this table depends on them)
                                cur.execute(f"DROP FUNCTION IF EXISTS {schema_dot}notify_{name}")

                            # handle custom notifications
                            if tbl["custom_notify_function_payload"] != "":
                                cusnotargs = tbl["custom_notify_function_payload"]
                                cur.execute(f"CREATE OR REPLACE FUNCTION {schema_dot}notify_{name}() RETURNS TRIGGER AS $$ BEGIN PERFORM pg_notify(TG_ARGV[0],{cusnotargs}); RETURN NULL; END; $$ LANGUAGE plpgsql")
                                cur.execute(f"CREATE OR REPLACE TRIGGER {name}_changed_custom AFTER INSERT OR UPDATE ON {schema_dot}{name} FOR EACH ROW EXECUTE FUNCTION {schema_dot}notify_{name} ('{schema_ul}{name}')")

                        # now that all triggers have been dropped, we can drop the functions for them
                        if redo_notify:
                            for tbl in tables:
                                if tbl["schema"] == "":
                                    schema_dot = ""
                                else:
                                    schema_dot = f'{tbl["schema"]}.'
                                # drop the standard functions
                                cur.execute(f"DROP FUNCTION IF EXISTS {schema_dot}notify_id()")
                                cur.execute(f"DROP FUNCTION IF EXISTS {schema_dot}notify_big()")

                        # now we can remake and re-attach the standard functions
                        for tbl in tables:
                            if raw_site_index == 0:
                                name = tbl["name"]
                            else:  # this is a raw data table, put a site index number in its name
                                name = f'{tbl["name"]}_{raw_site_index}'
                            if tbl["schema"] == "":
                                schema_dot = ""
                                schema_ul = ""
                            else:
                                schema_dot = f'{tbl["schema"]}.'
                                schema_ul = f'{tbl["schema"]}_'
                            cur.execute(f"CREATE OR REPLACE FUNCTION {schema_dot}notify_id() RETURNS TRIGGER AS $$ BEGIN PERFORM pg_notify(TG_ARGV[0], NEW.id::text); RETURN NULL; END; $$ LANGUAGE plpgsql")
                            cur.execute(f"CREATE OR REPLACE FUNCTION {schema_dot}notify_big() RETURNS TRIGGER AS $$ BEGIN PERFORM pg_notify(TG_ARGV[0], row_to_json(NEW)::text); RETURN NULL; END; $$ LANGUAGE plpgsql")
                            if tbl["notify_id"]:
                                cur.execute(f"CREATE OR REPLACE TRIGGER {name}_changed_id AFTER INSERT OR UPDATE ON {schema_dot}{name} FOR EACH ROW EXECUTE FUNCTION {schema_dot}notify_id ('{schema_ul}{name}')")
                                cur.execute(f"ALTER TABLE {schema_dot}{name} ENABLE ALWAYS TRIGGER {name}_changed_id")  # make sure triggers fire on replicas too
                            if tbl["notify_big"]:
                                cur.execute(f"CREATE OR REPLACE TRIGGER {name}_changed_big AFTER INSERT OR UPDATE ON {schema_dot}{name} FOR EACH ROW EXECUTE FUNCTION {schema_dot}notify_big ('{schema_ul}{name}')")
                                cur.execute(f"ALTER TABLE {schema_dot}{name} ENABLE ALWAYS TRIGGER {name}_changed_big")  # make sure triggers fire on replicas too
                            if raw_site_index == 0:  # another publication that has no raw data included
                                cur.execute(f"ALTER PUBLICATION non_raw_pub ADD TABLE {schema_dot}{name}")

            except Exception as e:
                self.lg.debug(f"Problem setting up db: {repr(e)}")
                ret = -1
            else:
                ret = 0
        else:
            ret = -1
        return ret
